module.exports = function(grunt){
  grunt.initConfig({
    clean: {
      options: { force: true },
      build: ['/dist/']
    },
    mkdir: {
      options: { force: true },
      create: ['dist', '/dist/css']
    },
    copy: {
      options: { force: true },
      main: {
        files: [
          {expand: true, cwd: '/template/', src: ['*'], dest: '../dist/'}
        ]
      }
    },
    bundle: {
      options: { force: true },
      target: {
        type: '.css',
        src:' /css/main.css',
        dest: '/dist/css/main.css'
      }
    },
    browserify: {
      build: {
        src: '/scripts/*.js',
        dest: '/dist/index.js'
      }
    },
    uglify: {
      build: {
        src: '/dist/index.js',
        dest: '/dist/bundle.min.js'
      }
    },
    watch: {
      options:{livereload:true},
      scripts: {
        files: ['/scripts/*'],
        tasks: ['clean', 'bundle', 'browserify', 'uglify', 'copy'],
        options: {
          spawn: false
        },
      },
    },
    express: {
      all:{
        options:{
          port:3000,
          hostname:'localhost',
          bases:['./dist'],
          livereload: true
        }
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-bundle');
  grunt.loadNpmTasks('grunt-mkdir');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-browserify');
  grunt.loadNpmTasks('grunt-express');
  grunt.loadNpmTasks('grunt-livereload');

  grunt.registerTask('default', ['clean', 'mkdir', 'bundle', 'browserify', 'uglify', 'copy']);
  grunt.registerTask('server', ['express', 'watch']);
};
