var mymap = L.map('map').setView([-40.54720023, 172.46337891], 5);
// hamilton - [-37.82544653, 175.24085999], 6);
L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=<YOUR API KEY HERE>', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    zoom: 2,
    id: 'mapbox.streets',
    accessToken: '<YOUR API KEY HERE>'
}).addTo(mymap);

console.log(L.VectorMarkers);

var markers = L.markerClusterGroup(
  {
    removeOutsideVisibleBounds: true,
    animate: true,
    disableClusteringAtZoom: 9,
    zoomToBoundsOnClick: true,
    showCoverageOnHover: false,

    iconCreateFunction: function(cluster){
      var markers = cluster.getAllChildMarkers();

      // get cindex avg
      var n = 0;
      for(var i = 0; i < markers.length; i++){
        var marker = markers[i];
        //console.log(marker.vizdata);
        n = n + marker.vizdata.cindex;
      }
      var avg = (n/markers.length).toFixed(2);
      var redMarker = L.VectorMarkers.icon({
          icon: 'coffee',
          markerColor: 'red'
        });
      return redMarker;

      // return L.divIcon({
      //   html: avg + "(" + markers.length + ")",
      //   className: "customMarker",
      //   bgPos: L.point(0, 0),
      //   iconSize: L.point(20, 20)
      // });
    }

  }
);

$(function(){

  $.getJSON("data/data100.json", function(data){

    for (var i = 0; i < data.length; i++) {
      var dataItem = data[i];
      var lat = dataItem.latlong[0];
      var lng = dataItem.latlong[1];
      // var marker1 = L.marker(new L.LatLng(lat, lng));
      // marker1.bindPopup(i + ' marker1');
      // markers.addLayer(marker1, { alt: i + " actual marker" });

      var bounds = getGridLatLng(lat, lng);
      L.rectangle(bounds, {color: "#FF7800", weight: 2}).addTo(mymap);
      var marker2 = L.marker(new L.LatLng(bounds[0][0], bounds[0][1]));
      marker2.vizdata = {"id": dataItem.id, "cindex": dataItem.cindex};
      markers.addLayer(marker2);
      marker2.bindPopup(i + ' bb marker');

    }
    mymap.addLayer(markers);

  });

});

function getGridLatLng(lat, lng){
  // lat = x, long = y
  const nzbb = {north: -34.25,  west:  166.25,  south: -47.35,  east:  178.65}
  const LATKM = -1/110.544;
  const LONGKM = LATKM * 1.2;

  // determine cell that meter is in
  var x = nzbb.north;
  //console.log(x, lat, LATKM);
  while(x > lat){
    x = x + LATKM;
  }
  if (x < LATKM) x = x - LATKM;

  var y = nzbb.west;
  while(y < lng){
     y = y - LONGKM;
  }
  if (y > LONGKM) y = y + LONGKM;

  return [[x, y], [x + LATKM, y - (LONGKM)]];
}
