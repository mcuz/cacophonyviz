function getRGB(pc, hex1, hex2){

  var rgb1 = convRGB(hex1);
  var rgb2 = convRGB(hex2);
  var result = interpolateColor(rgb1, rgb2, pc);
  result = RGBtoHex(result);
  //console.log(hex1, hex2, rgb1, rgb2, result);

  return result;

  function convRGB(hex) {
      var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
      return result ? [
          parseInt(result[1], 16),
          parseInt(result[2], 16),
          parseInt(result[3], 16)
      ] : null;
  }

  function RGBtoHex(rgb) {
      return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
  }

  function interpolateColor(color1, color2, factor) {
    if (arguments.length < 3) { factor = 0.5; }
    var result = color1.slice();
    for (var i=0;i<3;i++) {
      result[i] = Math.round(result[i] + factor*(color2[i]-color1[i]));
    }
    //console.log("interpolateColor " + result);
    return result;
  };

}
