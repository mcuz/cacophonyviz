const MAXMETERS = 200;
const SHOWBB = false;
const SHOWNZBB = false;


function initMap() {
  //var nz = {lat: -40.54720023, lng: 172.46337891};
  var nz = {lat:-37.78991900, lng:175.27875100};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 12,
    center: nz
  });

  $.getJSON("data/data.json", function(data){
    var bb = data.data;

    if (SHOWBB){
      $.each(bb, function(key, val){
        var rect = new google.maps.Rectangle({
          strokeColor: '#FF0000',
          strokeWeight: 2,
          map: map,
          bounds: {
            north: val.b1[0],
            south: val.b2[0],
            east:  val.b2[1],
            west:  val.b1[1]
          }
        });
      });
    }

    // nz bounding box -32.84267363,165.98144531, -47.45780853,179.34082031
    if (SHOWNZBB){
      var rect = new google.maps.Rectangle({
        strokeColor: '#FF0000',
        strokeWeight: 2,
        map: map,
        bounds: {
          north: -34.25,
          south: -47.35,
          west:  166.25,
          east:  178.65
        }
      });
    }

    var jsondata = [];

    for(var i = 0; i <  MAXMETERS; i++){
      var latlng = getRandomLatLong(bb[getRandomInt(0, bb.length)]);
      var rindex = getRandom(0.0, 1.0);
      rindex = parseFloat(rindex.toFixed(2));
      var dataItem = {
        "id" : i + 1 ,
        "latlong" : latlng,
        "cindex" : rindex
      };
      jsondata.push(dataItem);
    }

    console.log(JSON.stringify(jsondata));


  });

}


function getRandomInt(min, max){
  var num = Math.floor(getRandom(min, max));
  return num;
}

function getRandomLatLong(bbox){
  var lat = getRandom(bbox.b1[0], bbox.b2[0]);
  var lng = getRandom(bbox.b1[1], bbox.b2[1]);
  return [lat, lng];
}

function getRandom(min, max){
  if (max < min) { var t = min; min = max; max = t; }
  return Math.random() * (max - min) + min;
}

// given latlng return the topleft latlong of the 1KM cell in grid
function getGridLatLng(lat, lng){
  // lat = x, long = y
  const nzbb = {north: -34.25,  west:  166.25,  south: -47.35,  east:  178.65}
  const LATKM = -1/110.544;
  const LONGKM = LATKM * 1.2;

  // determine cell that meter is in
  var x = nzbb.north;
  //console.log(x, lat, LATKM);
  while(x > lat){
    x = x + LATKM;
  }
  if (x < LATKM) x = x - LATKM;

  var y = nzbb.west;
  while(y < lng){
     y = y - LONGKM;
  }
  if (y > LONGKM) y = y + LONGKM;

  return [[x, y], [x + LATKM, y - (LONGKM)]];
}
