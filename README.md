# README

## CaphonyViz
The intention is to create a suite of data visualisations both public and private.  Public visualisations to act as an engagement tool with the
general public and to harness citizen science contributions for project.  Private for any visualisations that may be sensitive in any way.

## Current Work  
Main focus has been on the development of a GIS cacophony index (cindex) visualisation.  Cindex simply taken as value in 0.0 - 1.0.  A working prototype
is available.

## API keys
all api keys have been removed and replaced with \<YOUR API KEY HERE\> in relevant parts of code. examine main.js file in each prototype.

## Server
The visualisation must be served via a web server to operate (local server for development)

## Breakdown of Coding efforts
  * Data generator - generates random data in json format that falls within new zealand geographical boundaries for testing visualisations.  Data output at browser console.  Note that this uses googlemaps API
  * prototype 1 - test of google maps with d3js overlay
  * prototype 2 - test of leaflet js zoom feature, addition of jquery and bootstrap libraries (for future use) to check for possible conflicts
  * prototype 3 - attempt to create grunt file and use installation of js libs via node packages (unsuccessfully)
  * prototype 4 - current working prototype uses custom map markers (leaflet vector markers library) with clustering.  A cluster is shown as yellow center, individual cacophonometer measurements shown as white
Green border gradient used to show cacophonometer index reading (0.0 dark green - 1.0 light green).  clustering at various levels of zoom interaction.
numbers show cacophony index and for a cluster second number shows number of cacophonometer readings in clustering
  * Sandpit - quick code experiments

## TODO
clean up prototype and build basic visualisation suite web interface which could then be shared with public as promotional type tool as no real data yet
investigate data limitations on free API keys.
